import 'package:flutter/material.dart';
import 'package:music_application/Gallery.dart';
import 'package:music_application/Home.dart';
import 'package:music_application/Player.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
      // home: Gallery(),
      //home: Player(),
    );
  }
}

import 'package:flutter/material.dart';

class Player extends StatefulWidget {
  const Player({super.key});

  @override
  State<Player> createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 60,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(19, 19, 19, 1),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Row(
          children: [
            Row(
              children: [
                const SizedBox(
                  width: 40,
                ),
                Column(
                  children: [
                    Container(
                      height: 21,
                      width: 22.6,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/image13.png'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'Favorite',
                      style: TextStyle(
                        color: Color.fromRGBO(157, 178, 206, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 40,
                ),
                Column(
                  children: [
                    Container(
                      height: 21,
                      width: 22.6,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/image11.png'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'Search',
                      style: TextStyle(
                        color: Color.fromRGBO(157, 178, 206, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 40,
                ),
                Column(
                  children: [
                    Container(
                      height: 21,
                      width: 22.6,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/image10.png'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'Home',
                      style: TextStyle(
                        color: Color.fromRGBO(157, 178, 206, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 40,
                ),
                Column(
                  children: [
                    Container(
                      height: 21,
                      width: 22.6,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/image12.png'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'Cart',
                      style: TextStyle(
                        color: Color.fromRGBO(157, 178, 206, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 40,
                ),
                Column(
                  children: [
                    Container(
                      height: 21,
                      width: 22.6,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/image14.png'),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'Profile',
                      style: TextStyle(
                        color: Color.fromRGBO(157, 178, 206, 1),
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
      backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
      body: Column(
        children: [
          Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                height: 450,
                width: 500,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/image15.png'),
                      fit: BoxFit.cover),
                ),
              ),
              const Positioned(
                bottom: 20,
                left: 130,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Alone in the Abyss',
                      style: TextStyle(
                        color: Color.fromRGBO(230, 154, 21, 1),
                        fontWeight: FontWeight.w400,
                        fontSize: 24,
                      ),
                    ),
                    Text(
                      'Youlakou',
                      style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        fontWeight: FontWeight.w600,
                        fontSize: 13,
                      ),
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 270,
                        ),
                        SizedBox(
                          height: 25,
                          width: 25,
                          child: Image(
                              image: AssetImage('assets/images/image16.png')),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Row(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
              ),
              Text(
                'Dynamic Warmup |',
                style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
              SizedBox(
                width: 280,
              ),
              Text(
                '4 min',
                style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 6,
            width: 450,
            child: LinearProgressIndicator(
              value: 0.5,
              // valueColor: AlwaysStoppedAnimation(
              //   Color.fromRGBO(230, 154, 21, 1),
              // ),
              backgroundColor: Color.fromRGBO(217, 217, 217, 0.19),
              color: Color.fromRGBO(230, 154, 21, 1),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: 20,
                width: 20,
                child: Image.asset('assets/images/image17.png'),
              ),
              Container(
                height: 20,
                width: 20,
                child: Image.asset('assets/images/image21.png'),
              ),
              Container(
                height: 50,
                width: 50,
                child: Image.asset('assets/images/image19.png'),
              ),
              Container(
                height: 20,
                width: 20,
                child: Image.asset('assets/images/image20.png'),
              ),
              Container(
                height: 20,
                width: 20,
                child: Image.asset('assets/images/image18.png'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
